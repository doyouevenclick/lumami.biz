# SpiroStack

Lumami Software provides paid support and commercial licensing options for the
SpiroStack software.

If this is something you are interested, contact us at [hello@lumami.biz](mailto:hello@lumami.biz) so we may discuss your interests and needs.

## Support

Commercial support includes:

* Training
* Installation
* Customization
* Consultation

## Licensing

If, for some reason, your company's use of SpiroStack is incompatible with the
terms of the AGPL, we have options.

The standard license allows:

* Use of licensed components in support of business services
* Use of licensed components in support of public-facing services
* Modification of components for internal use
* Release of modifications under the terms of the AGPL

The standard license does not allow:

* Use of licensed components in a Salt-oriented product
* Reselling of components
* Reselling of modified components

This license is designed to free businesses from the obligations of the AGPL
while preserving the community.
