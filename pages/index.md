<style>
section {
    display: inline-block;
    vertical-align: text-top;
    width: 15em;
    border: thick solid #000;
    margin: 1em;
    padding: 1em;
    border-radius: 0.5em;
    color: #fff;
    background-color: #202020;
}

section > h2:first-child {
    margin-top: 0;
}

section h2 {
    color: #fff;
    font-family: "Abril Fatface", serif;
}
</style>

Lumami Software provides expertise in Web, DevOps, and Systems. We specialize in:

<div style="text-align: center">
<section>
<h2>Server Administration</h2>

Linux Server administration, setup, and installation, including configuration
management.
</section>

<section>
<h2>Software Development</h2>

Business Databases, microservices, business automation, or anything else, with over
fifteen years of programming experience.
</section>

<section>
<h2>DevOps Integration</h2>

Testing pipelines, deployment tooling, or just some git training.
</section>
</div>


If you'd like to know more, contact us at [hello@lumami.biz](mailto:hello@lumami.biz)
